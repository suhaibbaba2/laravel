<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function create(){
        return view('application_form');
    }
    public function createForm(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'description' => 'required',
        ]);

        $model=new Test;
        $model->name=$request->input('name');
        $model->description=$request->input('description');
        $model->save();
        return redirect()->route('home');

    }

    public function show(){
        $model=Test::all();

        return view('show',['model'=>$model]);
    }
}
