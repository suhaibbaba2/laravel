<style>
    input{
        margin-bottom: 10px !important;
        text-align: center;
        text-transform: capitalize;
    }
</style>
@extends('layouts.app')

@section('content')
    <div class="container center-block text-center">
        <form method="post" action="/create">
        {{ csrf_field() }}
        <input type="text" class="form-control" name="name"          placeholder="name">
        <input type="text" class="form-control"  name="description"   placeholder="description">
        <input type="submit"  class="btn btn-success" name="submit">
    </form>
    </div>
@endsection