@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
                <div class="center-block text-center" style="margin-bottom: 20px">
                    <a href="{{URL::to('/')}}/admin/create" class="btn btn-primary">Create Form</a>
                    <a href="{{URL::to('/')}}/user/show"   class="btn btn-success">View Data</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
