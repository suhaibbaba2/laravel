<style>
    .table td ,
    .table th {
        text-align: center;
    }
</style>
@extends('layouts.app')

@section('content')
    <div class="container center-block">
        <table class="table table-condensed text-center">
            <thead>
              <tr>
                <th>Name</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
                @foreach($model as $row)
                    <tr>
                        <td>{{$row->name}}</td>
                        <td>{{$row->description}}</td>
                    </tr>
                 @endforeach
            </tbody>
        </table>
    </div>
@endsection